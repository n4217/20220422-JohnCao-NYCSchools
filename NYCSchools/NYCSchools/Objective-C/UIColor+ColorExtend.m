//
//  UIColor+CCCExtend.m
//  NYCSchools
//
//  Created by John Cao on 4/23/22.
//
//  This file contains Objective-C code. The Purpose is to show that I can mix Objective-C and Swift codes.
//
//  This Objective-C category provides function to convert hex string to UIColor.
//

#import "UIColor+ColorExtend.h"

@implementation UIColor(ColorExtend)

/// convert hex string to UIColor
///
/// - Parameter value:
///  - hexString: hex string which represents color
/// - Returns:
///  - UIColor: return UIColor
+ (UIColor *)makeColorByHex:(NSString *) hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                        [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                        [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                        [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

@end
