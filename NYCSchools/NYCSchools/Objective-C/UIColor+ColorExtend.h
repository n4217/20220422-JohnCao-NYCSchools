//
//  UIColor+CCCExtend.h
//  NYCSchools
//
//  Created by John Cao on 4/23/22.
//
//  This file contains Objective-C code. The Purpose is to show that I can mix
//  Objective-C and Swift codes.
//
//  This Objective-C category provides function to convert hex string to UIColor.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor(ColorExtend)

//  convert hex string to UIColor
+ (UIColor *)makeColorByHex:(NSString *) hexString;

@end
