//
//  NYCSchoolListManager.swift
//  NYCSchools
//
//  Created by John Cao on 4/23/22.
//
//  This is view model for school list screen. It will retrieve NYC high school list.
//

import Foundation
import UIKit

enum NYCSchoolNetworkError: Error {
    case networkFailure(Error)
    case invalidData
    case invalidStatusCode
    case wrongUrl
}

typealias CompletionHandler = (Result<[NYCSchool], NYCSchoolNetworkError>) -> Void

class NYCSchoolListManager {
    #if TEST
        // Only used for unit test cases
        static var shared: NYCSchoolListManager!
        override init() {}
    #else
        static let shared = NYCSchoolListManager()
        private init(){}
    #endif
    
    /// initiate school list view controller
    ///
    /// - Parameter value:
    ///  -
    /// - Returns:
    ///  - UIViewController: return NYCSchoolListViewController
    func getViewController() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "nycSchoolListViewControllerId") as! NYCSchoolListViewController
        return viewController
    }
    
    /// fetch school list from remote
    ///
    /// - Parameter value:
    ///  - completion: completion handler
    /// - Returns:
    ///  -
    func fetchSchools(completion: @escaping CompletionHandler) {
        //  retrieve URL from plist file
        //  sample url: https://data.cityofnewyork.us/resource/s3k6-pzi2.json
        guard let url = Bundle.main.object(forInfoDictionaryKey: "SchoolUrl") as? String,
              let schoolUrl = URL(string: url) else {
                  completion(.failure(.wrongUrl))
            return
        }
        //  use dataTask to make remote server call
        let task = URLSession.shared.dataTask(with: schoolUrl) { result in
            switch result {
            case .success(let data):
                do {
                    //  decode data to struct object
                    let decoder = JSONDecoder()
                    let schools = try decoder.decode([NYCSchool].self, from: data)
                    //  return sorted school list
                    completion(.success(schools.sorted { $0.school_name ?? "" < $1.school_name ?? "" }))
                } catch _ {
                    completion(.failure(.invalidData))
                }
            case .failure(let error):
                completion(.failure(.networkFailure(error)))
            }
        }
        task.resume()
    }

}

