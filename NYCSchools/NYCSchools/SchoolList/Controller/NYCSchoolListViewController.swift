//
//  NYCSchoolListViewController.swift
//  NYCSchools
//
//  Created by John Cao on 4/23/22.
//
//  This is school list view controller. It will display NYC high school list.
//

import UIKit

class NYCSchoolListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    
    private var schoolList: [NYCSchool]?
    private var schoolListCopy: [NYCSchool]?
    private var searchDelayAtionTimer: Timer?
    private let searchWaitTime = 0.5

    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    @IBOutlet weak var schoolTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationItem()
        setupView()
        setupAccessibility()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        retrieveSchools()
    }
    
    //  set up view
    private func setupView() {
        title = Constants.kHomeScreenTitle
        //  set up search bar
        searchBar.delegate = self
        //  set up school table view
        schoolTableView.dataSource = self
        schoolTableView.delegate = self
        schoolTableView.register(UINib.init(nibName: "NYCSchoolListCellView", bundle: Bundle.main), forCellReuseIdentifier: "nycSchoolListCellViewCell")
        schoolTableView.separatorStyle = .none
        //  set up progress indicator
        progressIndicator.isHidden = true
        progressIndicator.hidesWhenStopped = true
        view.bringSubviewToFront(progressIndicator)
    }
    
    //  set up accessibility ADA
    private func setupAccessibility() {
        searchBar.isAccessibilityElement = true
        searchBar.accessibilityLabel = Constants.kSearchBarHint
    }
    
    //  set up left navigation item
    private func setupNavigationItem() {
        //  set up navigation back button
        let backButton = UIBarButtonItem(
            title: "   ",
            style: .plain,
            target: nil,
            action: nil
        )
        backButton.width = 44
        navigationItem.backBarButtonItem = backButton
    }
    
    //  retrieve school list from remote
    private func retrieveSchools() {
        progressIndicator.isHidden = false
        progressIndicator.startAnimating()
        NYCSchoolListManager.shared.fetchSchools() { result in
            DispatchQueue.main.async {
                //  use main queue to refresh screen
                self.progressIndicator.stopAnimating()
                switch result {
                case .success(let schools):
                    self.schoolList = schools
                    //  save a copy of list for search
                    self.schoolListCopy = schools
                    self.schoolTableView.reloadData()
                case .failure(let error):
                    //  display error
                    self.displayAlert(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }
    
    //  below to check memory leak
    deinit {
        #if DEBUG
            print("deinit - NYCSchoolListViewController")
        #endif
    }
    
    //  MARK: tableView delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let schools = schoolList {
            return schools.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nycSchoolListCellViewCell")! as! NYCSchoolListCellView
        if let schools = schoolList {
            //  display school name, address and phone
            cell.nameLabel.text = schools[indexPath.row].school_name
            if let addressImage = UIImage(named: "address"),
               let phoneImage = UIImage(named: "call") {
                cell.addressLabel.addLeading(image: addressImage, text: schools[indexPath.row].primary_address_line_1 ?? "")
                cell.phoneLabel.addLeading(image: phoneImage, text: schools[indexPath.row].phone_number ?? "")
            } else {
                cell.addressLabel.text = schools[indexPath.row].primary_address_line_1
                cell.phoneLabel.text = schools[indexPath.row].phone_number
            }
        }
        //  set up accessibilty ADA
        cell.isAccessibilityElement = true
        cell.accessibilityLabel = (cell.nameLabel.text ?? "no name") + "address: " + (cell.addressLabel.text ?? "no address") + "Phone: " + (cell.phoneLabel.text ?? "no phone")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //  if select a school, go to Detail screen
        if let schools = schoolList {
            let school = schools[indexPath.row]
            let viewController = NYCSchoolDetailManager.shared.getViewController(school: school)
            if let nav = self.navigationController {
                nav.pushViewController(viewController, animated: true)
            }
        }
    }
    
    //  MARK: UISearchBarDelegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            //  if clean search text, refresh screen
            schoolList = schoolListCopy
            schoolTableView.reloadData()
        } else {
            //  delay some time before run actual search
            if let delayTimer = searchDelayAtionTimer {
                delayTimer.invalidate()
            }
            searchDelayAtionTimer = Timer(timeInterval: searchWaitTime, target: self, selector: #selector(delaySearch), userInfo: nil, repeats: false)
            if let delayTimer = searchDelayAtionTimer {
                RunLoop.main.add(delayTimer, forMode: .default)
            }
        }
        
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    //  search school from school list
    @objc func delaySearch() {
        if let delayTimer = searchDelayAtionTimer {
            delayTimer.invalidate()
            searchDelayAtionTimer = nil
        }
        filterSchoolList()
    }
    
    //  filter school per name, address, or phone
    private func filterSchoolList() {
        if let searchString = searchBar.text,
            let schools = schoolListCopy {
            schoolList = schools.filter({($0.school_name ?? "").lowercased().contains(searchString.lowercased())
                || ($0.primary_address_line_1 ?? "").lowercased().contains(searchString.lowercased())
                || ($0.phone_number ?? "").lowercased().contains(searchString.lowercased())})
            schoolTableView.reloadData()
        }
    }
    
}
