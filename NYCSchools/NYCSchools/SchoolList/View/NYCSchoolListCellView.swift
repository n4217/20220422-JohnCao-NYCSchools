//
//  NYCSchoolListCellView.swift
//  NYCSchools
//
//  Created by John Cao on 4/23/22.
//
//  This is cell for school list table view.
//

import UIKit

class NYCSchoolListCellView: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //  set up view, shadown and corner radius etc.
    private func setupView() {
        contentView.layer.borderWidth = 0
        contentView.layer.cornerRadius = 5.0
        containerView.layer.cornerRadius = 5.0
        containerView.layer.borderWidth = 0
        containerView.layer.shadowColor = UIColor.make(byHex: Constants.kShadowColorString).cgColor
        containerView.layer.shadowRadius = 5.0
        containerView.layer.shadowOpacity = 0.2
        containerView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
    }
    
}
