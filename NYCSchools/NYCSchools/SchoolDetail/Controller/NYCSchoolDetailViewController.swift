//
//  NYCSchoolDetailViewController.swift
//  NYCSchools
//
//  Created by John Cao on 4/23/22.
//
//  This is detail view controller. It will display NYC high school details and SAT test scores for that school.
//

import UIKit
import MapKit

class NYCSchoolDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var detailTableView: UITableView!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    var school: NYCSchool?
    private var schoolData = [Int:[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //  set up view
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //  retrieve school detail
        retrieveSchoolDetail()
    }
    
    //  below to make sure there is no memory leak
    deinit {
        #if DEBUG
            print("deinit - NYCSchoolDetailViewController")
        #endif
    }
    
    private func setupView() {
        title = Constants.kDetailScreenTitle
        //  set up tableView
        detailTableView.dataSource = self
        detailTableView.delegate = self
        detailTableView.register(UINib.init(nibName: "NYCSchoolDetailCellView", bundle: Bundle.main), forCellReuseIdentifier: "nycSchoolDetailCellViewCell")
        //  set up progress indicator
        progressIndicator.isHidden = true
        progressIndicator.hidesWhenStopped = true
        view.bringSubviewToFront(progressIndicator)
    }
    
    private func retrieveSchoolDetail() {
        if let sch = school {
            nameLabel.text = sch.school_name
            //  retrieve SAT scores
            retrieveSatScore(school: sch)
        }
    }
    
    //  retrieve SAT score
    private func retrieveSatScore(school: NYCSchool) {
        progressIndicator.isHidden = false
        progressIndicator.startAnimating()
        //  use async/await to fetch SAT score
        Task(priority: .medium) {
            do {
                schoolData = try await NYCSchoolDetailManager.shared.fetchSATScore(school: school)
                DispatchQueue.main.async {
                    //  use main queue to refresh screen
                    self.detailTableView.reloadData()
                    self.markMap()
                    self.progressIndicator.stopAnimating()
                }
            } catch {
                DispatchQueue.main.async {
                    //  use main queue to display error alert
                    self.progressIndicator.stopAnimating()
                    self.displayAlert(title: "Error", message: error.localizedDescription)
                }
            }
        }
        
    }
        
    //  mark school in the map
    private func markMap() {
        if let sch = school {
            let latDelta:CLLocationDegrees = 0.01
            let longDelta:CLLocationDegrees = 0.01
            if let schoolLatString = sch.latitude, let schoolLat = Double(schoolLatString),
               let schoolLongString = sch.longitude, let schoolLong = Double(schoolLongString) {
                let theSpan:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
                let pointLocation:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: schoolLat, longitude: schoolLong)

                let region:MKCoordinateRegion = MKCoordinateRegion(center: pointLocation, span: theSpan)
                    mapView.setRegion(region, animated: true)

                let pinLocation : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: schoolLat, longitude: schoolLong)
                let annotation = MKPointAnnotation()
                annotation.coordinate = pinLocation
                annotation.title = sch.school_name
                mapView.addAnnotation(annotation)
            }
        }
    }
    
    //  MARK: tableView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //  There are two sections: one for SAT, one for school details
        if schoolData.count > 0 {
            return schoolData[section]?.count ?? 0
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //  set up cell to display either SAT or school detail
        let cell = tableView.dequeueReusableCell(withIdentifier: "nycSchoolDetailCellViewCell")! as! NYCSchoolDetailCellView
        if schoolData.count > 0,
           let data = schoolData[indexPath.section] {
            cell.fieldNameLabel.text = Array(data.keys)[indexPath.row] + ":"
            cell.fieldValueLabel.text = Array(data.values)[indexPath.row] as? String
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //  If select row, display that row content to alert popup.
        if schoolData.count > 0,
           let data = schoolData[indexPath.section] {
            let alertTitle = Array(data.keys)[indexPath.row]
            if let alertMessage = Array(data.values)[indexPath.row] as? String {
                self.displayAlert(title: alertTitle, message: alertMessage)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //  There will be two sections: SAT and School Details
        if schoolData.count > 0 {
            return schoolData.keys.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //  set title for section
        if section == 0 {
            return Constants.kDetailScreenHeader1
        } else {
            return Constants.kDetailScreenHeader2
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //  set color for title section
        guard let tableView = view as? UITableViewHeaderFooterView else { return }
        tableView.textLabel?.textColor = UIColor.white
        tableView.contentView.backgroundColor = UIColor.lightGray
    }
    
}
