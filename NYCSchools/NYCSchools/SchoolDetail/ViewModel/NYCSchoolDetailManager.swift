//
//  NYCSchoolDetailManager.swift
//  NYCSchools
//
//  Created by John Cao on 4/23/22.
//
//  This is view model for Detail view controller. Any data retrieveal and massage will be located in this file.
//

import Foundation

class NYCSchoolDetailManager {
    #if TEST
        // Only used for unit test cases
        static var shared: NYCSchoolDetailManager!
        override init() {}
    #else
        static let shared = NYCSchoolDetailManager()
        private init(){}
    #endif
    
    /// initiate school detail view controller
    ///
    /// - Parameter value:
    ///  - school: school object
    /// - Returns:
    ///  - UIViewController: return NYCSchoolDetailViewController
    func getViewController(school: NYCSchool) -> UIViewController {
        let storyboard = UIStoryboard(name: "NYCSchoolDetail", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "nycSchoolDetailViewControllerId") as! NYCSchoolDetailViewController
        viewController.school = school
        return viewController
    }
    
    /// fetch SAT scores from remote for a school
    ///
    /// - Parameter value:
    ///  - school: school object
    /// - Returns:
    ///  - Dictionary: return [Int:[String:Any]] which contains SAT scores
    func fetchSATScore(school: NYCSchool) async throws -> [Int:[String:Any]] {
        //  retrieve URL from plist file
        //  sample url: https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=31R080
        guard let url = Bundle.main.object(forInfoDictionaryKey: "SATUrl") as? String,
              let dbn = school.dbn,
              let satUrl = URL(string: url + dbn) else {
                  throw NYCSchoolNetworkError.wrongUrl
        }

        //  use async/await to retrieve data
        let satRequest = URLRequest(url: satUrl)
        let (data, response) = try await URLSession.shared.data(for: satRequest)
        //  check httpresponse status code
        if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
            throw NYCSchoolNetworkError.invalidStatusCode
        }
        //  decode data to struct
        do {
            let decoder = JSONDecoder()
            let satScores = try decoder.decode([NYCSchoolSATModel].self, from: data)
            //  convert struct to dictionary
            var schoolData = [Int:[String:Any]]()
            if satScores.count > 0 {
                //  convert SAT struct score to dictionary
                let satDictionary = try satScores[0].allProperties()
                schoolData[0] = satDictionary
            }
            //  add school struct to dictionary
            let schoolDictionary = try school.allProperties()
            schoolData[schoolData.count] = schoolDictionary
            return schoolData
        } catch {
            throw NYCSchoolNetworkError.invalidData
        }
    }
    
}
