//
//  NYCSchoolDetailCellView.swift
//  NYCSchools
//
//  Created by John Cao on 4/23/22.
//
//  This is Detail screen table view cell. It has two labels: one for field name and the other for field value.
//

import UIKit

class NYCSchoolDetailCellView: UITableViewCell {

    @IBOutlet weak var fieldNameLabel: UILabel!
    @IBOutlet weak var fieldValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
