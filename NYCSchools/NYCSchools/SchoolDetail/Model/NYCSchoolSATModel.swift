//
//  NYCSchoolSATModel.swift
//  NYCSchools
//
//  Created by John Cao on 4/23/22.
//
//  This is data model for SAT scores.
//

import Foundation

struct NYCSchoolSATModel: Codable, Loopable {
    let dbn: String?
    let school_name: String?
    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
    
    //  below is to use CodingKeys Strategy. If uncomment below, app will map field names to camel case. I tested and worked fine.
//    let dbn: String?
//    let schoolName: String?
//    let numberOfTestTakers: String?
//    let readingAvgScore: String?
//    let mathAvgScore: String?
//    let writingAvgScore: String?
//
//    private enum CodingKeys: String, CodingKey {
//        case dbn
//        case schoolName = "school_name"
//        case numberOfTestTakers = "num_of_sat_test_takers"
//        case readingAvgScore = "sat_critical_reading_avg_score"
//        case mathAvgScore = "sat_math_avg_score"
//        case writingAvgScore = "sat_writing_avg_score"
//    }
}

