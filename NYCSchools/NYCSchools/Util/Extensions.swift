//
//  Extensions.swift
//  NYCSchools
//
//  Created by John Cao on 4/23/22.
//
//  This file contains all extensions.
//

import Foundation
import UIKit

extension UILabel {
    /// add image before test for UILabel
    ///
    /// - Parameter value:
    ///  - image: leading image
    ///  - text: text for UILabel
    /// - Returns:
    ///
    func addLeading(image: UIImage, text:String) {
        let resizedImage = image.resize(targetSize: CGSize(width: 10, height: 10))
        let attachment = NSTextAttachment()
        attachment.image = resizedImage

        let attachmentString = NSAttributedString(attachment: attachment)
        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(attachmentString)
        
        let string = NSMutableAttributedString(string: " " + text, attributes: [:])
        mutableAttributedString.append(string)
        self.attributedText = mutableAttributedString
    }
}

extension UIImage {
    /// resize image
    ///
    /// - Parameter value:
    ///  - targetSize: final image size
    /// - Returns:
    ///  - UIImage: return resized image
    func resize(targetSize: CGSize) -> UIImage? {
        return UIGraphicsImageRenderer(size:targetSize).image { _ in
            self.draw(in: CGRect(origin: .zero, size: targetSize))
        }
    }
}

extension URLSession {
    /// custom dataTask with input url parameter
    ///
    /// - Parameter value:
    ///  - url: the url dataTask will call to
    ///  - handler: completion handler
    /// - Returns:
    ///  - URLSessionDataTask: return dataTask
    func dataTask(
        with url: URL,
        handler: @escaping (Result<Data, Error>) -> Void
    ) -> URLSessionDataTask {
        dataTask(with: url) { data, _, error in
            if let error = error {
                handler(.failure(error))
            } else {
                handler(.success(data ?? Data()))
            }
        }
    }
}


extension Loopable {
    /// Loop through struct object and map field name/value to dictionary
    ///
    /// - Parameter value:
    ///  -
    /// - Returns:
    ///  - Dictionary: [String: Any]
    func allProperties() throws -> [String: Any] {
        var result: [String: Any] = [:]
        let mirror = Mirror(reflecting: self)
        guard let style = mirror.displayStyle, style == .struct || style == .class else {
            throw NSError()
        }

        for (property, value) in mirror.children {
            guard let property = property else {
                continue
            }
            let newProperty = property.replacingOccurrences(of: "_", with: " ").capitalized
            result[newProperty] = value
        }

        return result
    }
}

extension UIViewController {
    /// Display alert popup
    ///
    /// - Parameter value:
    ///  - title: alert title
    ///  - message: alert message
    /// - Returns:
    ///  - 
    func displayAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}
