//
//  Constants.swift
//  NYCSchools
//
//  This file contains constant values
//

import Foundation

struct Constants {
    static let kHomeScreenTitle = "NYC High School List"
    static let kSearchBarHint = "Search name, address, or phone"
    static let kDetailScreenTitle = "School Details"
    static let kDetailScreenHeader1 = "Section 1 - SAT Scores"
    static let kDetailScreenHeader2 = "Section 2 - School Details"
    static let kShadowColorString = "000029"
}
