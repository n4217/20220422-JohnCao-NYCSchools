//
//  NYCSchoolListManagerTests.swift
//  NYCSchoolsTests
//
//  Created by John Cao on 4/23/22.
//
//  This file contains unit test for List view model.
//

import XCTest

@testable import NYCSchools

class NYCSchoolListManagerTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //  test getViewController()
    func testGetViewController() throws {
        let model = NYCSchoolListManager.shared
        let vc = model.getViewController()
        XCTAssert(vc is NYCSchoolListViewController)
    }
    
    //  test fetchSchools()
    func testFetchSchools() throws {
        let expect = expectation(description: "testFetchSchools")
        NYCSchoolListManager.shared.fetchSchools { result in
            switch result {
            case .success(let schools):
                XCTAssert(schools.count > 0)
            case .failure(let error):
                XCTFail("\(error)")
            }
            expect.fulfill()
        }
        //  Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 10) { error in
          if let err = error {
            XCTFail("\(err)")
          }
        }
    }

}
