//
//  NYCSchoolDetailManagerTests.swift
//  NYCSchoolsTests
//
//  Created by John Cao on 4/23/22.
//
//  This file contains unit test for Detail view model.
//

import XCTest

@testable import NYCSchools

class NYCSchoolDetailManagerTests: XCTestCase {
    
    private var schoolList: [NYCSchool]?

    override func setUp() {
        //  use expectation to set up object
        let expect = expectation(description: "testFetchSchools")
        NYCSchoolListManager.shared.fetchSchools { result in
            switch result {
            case .success(let schools):
                self.schoolList = schools
            case .failure(let error):
                print(error.localizedDescription)
            }
            expect.fulfill()
        }
        //  Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 10) { error in
          if let err = error {
              print(err.localizedDescription)
          }
        }
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //  test getViewController()
    func testGetViewController() throws {
        if let schools = schoolList {
            let model = NYCSchoolDetailManager.shared
            let vc = model.getViewController(school: schools[0])
            XCTAssert(vc is NYCSchoolDetailViewController)
        } else {
            XCTFail("No scchols")
        }
    }
    
    //  test fetchSATScores()
    func testFetchSATScore() async throws {
        if let schools = schoolList {
            let model = NYCSchoolDetailManager.shared
            let schoolData = try await model.fetchSATScore(school: schools[0])
            XCTAssert(schoolData.count > 0)
        } else {
            XCTFail("No scchols")
        }
    }

}
