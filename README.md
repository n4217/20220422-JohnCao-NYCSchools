# 20220422-JohnCao-NYCSchools



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/n4217/20220422-JohnCao-NYCSchools.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/n4217/20220422-JohnCao-NYCSchools/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# NYCSchools

## Name
NYCSchools.

## Description
This iOS app downloads a list of NYC High Schools data and display detail information, as well as SAT scores for a school.

## Design Pattern
The app mainly uses MVVM design pattern to separate of concern. Also singlton is used too.

## Important Notes
Some thoughts during coding and worth mentioning here:
- The app is built and compiled with XCode 13.2 and iOS 15.2. 
- Swift is primary language. Added Objective-C files (UIColor+ColorExtend.h & UIColor+ColorExtend.m) to show my skills in both languages.
- MVVM design pattern is used. Under SchoolList & SchoolDetail folder, there are model, view, view model and controller sub-folders.
- Singleton is used for NYCSchoolListManager and NYCSchoolDetailManager.
- Async/await is used to fetch School SAT scores in NYCSchoolDetailManager. Traditional @escaping closure is used to fetch NYC High School list.
- Codable protocol is used to decode JSON. CodingKeys strategy was tried and proved working.
- NYCSchoolListManager and NYCSchoolDetailManager are covered with unit testing cases.   


